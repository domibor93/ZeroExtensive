%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Zero sum games in extensive form     %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;
stages = 2; %number of stages
d=0; %how deep in tree
l=0; %which branch on given level

%D=[ 1 9 -5 7]; %saddle point %dziala ok
D=[ 8 -1 -2 3]; %bez saddle pointa
D=[ 1:12 D];
%D=[1:16]; % tu tez dziala ok
%D=[8 0 6 -6 1 9 -5 7 -10 3 7 10 4 -8 6 7]; %tu dziala ok
P=zeros(2*stages, 4^stages); %probability for each option 
P=P-1; %oznaczam sobie -1, cos w rodzaju maski
FP=P;
Size=size(D,2);

% checking if the input vector has proper size
if 4^stages>Size
    disp('To few input arguments');
    return;
end
if 4^stages < Size
    disp ('To many input arguments');
    return;
end

[Gm, P, d, l] = SolveStageZero(D, P, d, l); %for 2x2 games

%Final Probability FP - calculated basing on probability from higher levels
FP(1,:)=P(1,:);
disp(['Result of the game:  ' num2str(Gm)]);
for j=2:size(P,1)
    ile=1; %nr of elements in previous row
    while (P(j-1,ile+1)~=-1 && ile < size(P,2))
        ile=ile+1;
        if(ile+1>=size(P,2))
            ile=ile+1;
            break;
        end
    end
   % disp(ile);
    for t=1:ile
        FP(j,(2*t)-1)=P(j,(2*t)-1)*FP(j-1,t);
        FP(j,(2*t))=P(j,2*t)*FP(j-1,t);        
    end
   
end
    flag=1;
for j=1:size(P,1)
    indeks=1;
    while (P(j,indeks+1)~=-1 && indeks < size(P,2))
        indeks=indeks+1;
        if(indeks+1>=size(P,2))
            indeks=indeks+1;
            break;
        end
    end
    %disp(FP(j,1:indeks));
    if flag==1
    disp([ 'D1: ' num2str(FP(j,1:indeks))]);
    flag=2;
    else
    disp([ 'D2: ' num2str(FP(j,1:indeks))]);
    flag=1;
    end
end

