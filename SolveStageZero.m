%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Subfunction of Zero sum games-extensive  %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 function [Gm, P, d, l] = SolveStageZero(D, P, d, l)
%Solve one stage of game D1-> minimize
%Gm- game value
%Pi- probability
d=d+1;

if size(D,2)~=(4)
  for j=1:4
     [Dtmp(j), P, d, l]=SolveStageZero(D(1+4*(j-1):4+4*(j-1)), P, d, l); 
  end    
  d=d-1;
     [Gm, P, d, l]=SolveStageZero(Dtmp, P, d, l);
     d=d+1;
    
    
    
else %w�a�ciwa funkcja
    D=[D(1) D(2); D(3) D(4)]; %zapisywanie w postaci macierzowej
    [Min, j2] = min(D);
    [Max, i1] = max(D, [], 2);
    %j1 - wektor zawieraj�cy nr wiersza z min kolumny
    %i1 - wektor zawierajacy nr kolumny z max wiersza
    [~, i2] = max(Min, [], 2); % ~ zamiast MaxMin zeby sie Matlab nie plu�
    [MinMax, j1] = min(Max);
    %j1, i2 - liczby odpowiadajace wierszowi/kolumnie z minmaxem i maxminem

    a1=[ j1, i1(j1)]; %wspolrzedne maxmin (gracz D1)
    a2=[j2(i2), i2]; %wspolrzedne minmax (gracz D2)
    index=1; %znajdz w ktorym miejscu wpisywa� prawdopodobie�stwo
    index2=1; %2 rows for each stage
    while P(2*d,index)~=-1
    index=index+1;
    end
    while P(2*d-1,index2)~=-1
    index2=index2+1;
    end
    
    if a1(1)==a2(1) && a1(2)==a2(2) %sprawdzenie czy jest saddle point 
      Gm = MinMax; %= D( a1(1), a1(2) );
       if a1(1)==1 && a1(2)==1
          P(2*d-1,index2)=1;
          P(2*d-1,index2+1)=0; 
          P(2*d,index)=1;
          P(2*d,index+1)=0;
          P(2*d,index+2)=0;
          P(2*d,index+3)=0;
       end
       if a1(1)==2 && a1(2)==1
          P(2*d-1,index2)=0;
          P(2*d-1,index2+1)=1;
          P(2*d,index)=0;
          P(2*d,index+1)=0;
          P(2*d,index+2)=1;
          P(2*d,index+3)=0;
       end
       if a1(1)==1 && a1(2)==2
          P(2*d-1,index2)=1;
          P(2*d-1,index2+1)=0;
          P(2*d,index)=0;
          P(2*d,index+1)=1;
          P(2*d,index+2)=0;
          P(2*d,index+3)=0;
       end
       if a1(1)==2 && a1(2)==2
          P(2*d-1,index2)=0;
          P(2*d-1,index2+1)=1;
          P(2*d,index)=0;
          P(2*d,index+1)=0;
          P(2*d,index+2)=0;
          P(2*d,index+3)=1;       
       end                        
                     
       
                               
                    
    else                            %strategie mieszane
      [Gm, z, y] = Mixed2by2(D);
      %y(1)/y(2) = D1Left/D1Right
      %z(1)/z(2) = D2Left/D2Right
      %y - rozklad prawdopodobie�stwa dla D1
      %z - rozklad prawdopodobie�stwa dla D2
      P(2*d-1,index2)=y(1);
      P(2*d-1,index2+1)=y(2);
      P(2*d,index)=z(1);
      P(2*d,index+1)=z(2);
      P(2*d,index+2)=z(1);
      P(2*d,index+3)=z(2);      
    end
end
d=d-1;