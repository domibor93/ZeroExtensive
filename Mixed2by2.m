%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Mixed 2x2 games               %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Sm, z, y, chck] = Mixed2by2(D)
%chck - check if the value of the game is equal for both players
%constraints equation in matrix form for z:
a=[1 1; D(1,1)-D(1,2) D(2,1)-D(2,2)];
y=a\[1 ; 0]; %Solve systems of linear equations Ax = B for x
Sm1 = D(1,1)*y(1)+D(2,1)*y(2); %obliczanie warto�ci gry

%constraints equation in matrix form for y:
b=[1 1; D(1,1)-D(2,1) D(1,2)-D(2,2)];
z=b\[1 ; 0];
Sm2 =D(1,1)*z(1)+D(1,2)*z(2);
    chck = 1; %flaga kontrolna
    Sm=Sm1;

end